import React from 'react'
import { Menu, Dropdown, Image } from 'semantic-ui-react'
import NProgress from 'nprogress'
import Router from 'next/router'
import LinkWithRouter from './link-with-router/link-with-router'

Router.onRouteChangeStart = (url) => NProgress.start()
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()

const Header = () => (
  <Menu inverted fluid>
    <Menu.Menu position='left'>
      <LinkWithRouter text="Home" url="/" />
    </Menu.Menu>
    <Menu.Menu position='right'>
      <Dropdown 
        trigger={<span><Image avatar src='https://s3.amazonaws.com/uifaces/faces/twitter/petrangr/128.jpg' /> {'Emad Emami'}</span>}
        pointing='top left' 
        icon={null} 
      >
        <Dropdown.Menu>
          <Dropdown.Header icon='tags' content='Filter by tag' />
          <Dropdown.Divider />
          <Dropdown.Item icon='attention' text='Important' />
          <Dropdown.Item icon='comment' text='Announcement' />
          <Dropdown.Item icon='conversation' text='Discussion' />
        </Dropdown.Menu>
      </Dropdown>
    </Menu.Menu>
  </Menu>
)

export default Header