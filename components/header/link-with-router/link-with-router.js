import Link from 'next/link'
import { withRouter } from 'next/router'

const LinkWithRouter = ({text, url, router}) => (
	<Link href={url} prefetch>
		<a className={`item ${router.route === url ? 'active' : ''}`}>{text}</a>
	</Link>
)

export default withRouter(LinkWithRouter)