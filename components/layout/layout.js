import semanticStyles from 'semantic-ui-css/semantic.min.css'
import nprogressStyles from 'nprogress/nprogress.css'
import Header from '../header/header'

const Layout = (props) => (
    <div className="ui inverted vertical center aligned segment" style={{minHeight: '500px'}}>
      <style jsx global>{semanticStyles}</style>
      <style jsx global>{nprogressStyles}</style>
      <div className="ui large inverted pointing secondary menu">
        <div className="ui container">
          <Header />
        </div>
      </div>
      <div>
        {props.children}
      </div>
    </div>
  )
  
export default Layout